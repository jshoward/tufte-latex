# Tufte-LaTeX classes

A Tufte-inspired LaTeX class originally from [tufte-latex on Google Code](https://code.google.com/p/tufte-latex/)

> As discussed in the [Book Design thread](http://www.edwardtufte.com/bboard/q-and-a-fetch-msg?msg_id=0000hB) of Edward Tufte's [Ask E.T Forum](http://www.edwardtufte.com/bboard/), this site is home to LaTeX classes for producing handouts and books according to the style of Edward R. Tufte and Richard Feynman.
